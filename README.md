# Ray Casting

Ray Casting in C++

## What is Ray casting
The idea behind ray casting is to trace rays from the eye, one per pixel, and find the closest object blocking the path of that ray – think of an image as a screen-door, with each square in the screen being a pixel. This is then the object the eye sees through that pixel. Using the material properties and the effect of the lights in the scene, this algorithm can determine the shading of this object. The simplifying assumption is made that if a surface faces a light, the light will reach that surface and not be blocked or in shadow. The shading of the surface is computed using traditional 3D computer graphics shading models. . 

## Getting started

```
premake5 gmake2
make
./bin/rayCasting
```
## To Move
> Arrow Keys

## Dependencies
- SFML
- Premake 
- make 
- Xorg server
## Preview

![](./assets/ss2.gif)

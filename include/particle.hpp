#pragma once

#include "boundary.hpp"
#include "ray.hpp"
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <iostream>
#include <optional>

class Particle
{
public:
  sf::Vector2f origin;

  std::vector<ray> rays;
  Particle(float x, float y)
  {

    origin.x = x;
    origin.y = y;
    setRay(x, y);
  }
  void setRay(float x, float y)
  {
    for (int i = 0; i <= 25; i++)
    {
      sf::Vector2f temp(25, i);
      rays.emplace_back(x, y, temp);
    }
    for (int i = 25; i >= 0; i--)
    {
      sf::Vector2f temp(i, 25);
      rays.emplace_back(x, y, temp);
    }
    for (int i = 25; i >= 0; i--)
    {
      sf::Vector2f temp(-1 * i, 25);
      rays.emplace_back(x, y, temp);
    }
    for (int i = 25; i >= 0; i--)
    {
      sf::Vector2f temp(25, -1 * i);
      rays.emplace_back(x, y, temp);
    }
  }

  void mvRight()
  {
    if (this->origin.x > 0)
    {
      this->origin.x += 0.1;
    }
    rays.clear();
    std::cout << "the x : " << this->origin.x << "the y: " << this->origin.y << '\n';
    // adds the updated value to the vector
    setRay(this->origin.x, this->origin.y);
  }
  void mvLeft()
  {
    if (this->origin.x > 0)
    {
      this->origin.x -= 0.1;
    }
    rays.clear();
    std::cout << "the x : " << this->origin.x << "the y: " << this->origin.y << '\n';
    // adds the updated value to the vector
    setRay(this->origin.x, this->origin.y);
  }
  void mvDown()
  {
    if (this->origin.y > 0)
    {
      this->origin.y -= 0.1;
    }
    rays.clear();
    std::cout << "the x : " << this->origin.x << "the y: " << this->origin.y << '\n';
    // adds the updated value to the vector
    setRay(this->origin.x, this->origin.y);
  }

  void mvUp()
  {
    if (this->origin.y > 0)
    {
      this->origin.y += 0.1;
    }
    rays.clear();
    std::cout << "the x : " << this->origin.x << "the y: " << this->origin.y << '\n';
    // adds the updated value to the vector
    setRay(this->origin.x, this->origin.y);
  }

  void Cast(std::array<Boundary, 2> Boundaries, sf::RenderWindow &win) const
  {
    for (auto x : rays)
    {
      if (x.cast(Boundaries[0]))
      {
        sf::VertexArray line{sf::LinesStrip, 2};
        line[0].position = sf::Vector2f(x.cast(Boundaries[0]).value().x, x.cast(Boundaries[0]).value().y);
        line[1].position = origin;
        win.draw(line);
      }
      if (x.cast(Boundaries[1]))
      {
        sf::VertexArray line{sf::LinesStrip, 2};
        line[0].position = sf::Vector2f(x.cast(Boundaries[1]).value().x, x.cast(Boundaries[1]).value().y);
        line[1].position = origin;
        win.draw(line);
      }
    }
  }
};

#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include "boundary.hpp"
#include <iostream>
#include <optional>
#include <SFML/Window.hpp>

class ray
{

public:
    sf::Vector2f origin;
    sf::Vector2f direction; //
    sf::Vector2f point2;    // represents the second point on the line >

    // sets the origin of the rays
    ray(float x, float y, sf::Vector2f dir)
        : origin(x, y),
          direction(dir)
    {
        ;
    }

    std::optional<sf::Vector2f> cast(Boundary &wall)
    {
        float x1 = wall.point1.x;
        float y1 = wall.point1.y;
        float x2 = wall.point2.x;
        float y2 = wall.point2.y;

        float x3 = origin.x, y3 = origin.y;
        float x4 = direction.x + origin.x, y4 = direction.y + origin.y;

        float den = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
        if (den == 0)
        {
            return std::nullopt;
        }
        float t = ((x1 - x3) * (y3 - y4) - (y1 - y3) * (x3 - x4)) / den;
        float u = -1 * ((x1 - x2) * (y1 - y3) - (y1 - y2) * (x1 - x3)) / den;

        if (t > 0 && t < 1 && u > 0)
        {
            sf::Vector2f intersection{x1 + t * (x2 - x1), y1 + t * (y2 - y1)};
            return intersection;
        }
        else
        {
            return std::nullopt;
        }
    }
};
#ifndef BOUNDARY_H_
#define BOUNDARY_H_

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/Window.hpp>

class Boundary
{

  sf::VertexArray line{sf::LinesStrip, 2};

public:
  sf::Vector2f point1, point2;

  Boundary(float f1, float f2, float f3, float f4)
  {
    point1.x = f1;
    point1.y = f2;
    point2.x = f3;
    point2.y = f4;

    line[0].position = sf::Vector2f(f1, f2);
    line[1].position = sf::Vector2f(f3, f4);
  }

  void draw(sf::RenderWindow &win)
  {
    win.draw(line);
  }
};

#endif // BOUNDARY_H_

workspace " RayCasting"
configurations {"Debug", "Release"}
project "RayCasting"
kind "ConsoleApp"
cppdialect "C++17"
language "C++"
targetdir "bin/"
files {"src/**.cpp"}


links {"sfml-graphics", "sfml-window", "sfml-system"}

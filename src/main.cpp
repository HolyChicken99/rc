#include <iostream>
#include "../include/boundary.hpp"
#include "../include/ray.hpp"
#include "../include/particle.hpp"

int main()
{
  sf::RenderWindow window(sf::VideoMode(1000, 1000), "SFML works!");
  Boundary br(500, 0, 500, 500);
  Boundary br2(0, 700, 500, 700);
  std::array<Boundary, 2> Boundaries{br, br2};

  ray R(50, 20, sf::Vector2f{1, 0});
  sf::VertexArray line{sf::LinesStrip, 2};

  Particle part(400, 50);
  while (window.isOpen())
  {
    sf::Event event;
    while (window.pollEvent(event))
    {
      if (event.type == sf::Event::Closed)
        window.close();
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
    {
      part.mvRight();
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
    {
      part.mvLeft();
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
    {
      part.mvDown();
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
    {
      part.mvUp();
    }

    window.clear();
    part.Cast(Boundaries, window);
    br.draw(window);
    br2.draw(window);
    window.draw(line);
    window.display();
  }

  return 0;
}
